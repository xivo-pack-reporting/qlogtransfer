Run the tests
-------------
Have a local postgresql base testdata with a user test and password test

package
-------
sbt debian:packageBin

install 
-------
dpkg -i qlogtransfer-{version}.deb


Running
-------
sudo ./bin/qlogtransfer -Dconfig.file=./conf/qlogtransfer.conf -Dlogback.configurationFile=./conf/logger.xml

Modify index to activate ttl :
curl -XPUT 'http://192.168.51.157:9200/queuelogs/'

curl -XPUT 'http://192.168.51.157:9200/queuelogs/queuelogs/_mapping' -d '
{
    "queuelogs":{
        "_ttl": {"enabled": true, "default": "1d"},
        properties: {
            "queuedisplayname" :   {"type": "string", "index" : "not_analyzed"},
            "agentname" :          {"type": "string", "index" : "not_analyzed"},
            "groupname" :          {"type": "string", "index" : "not_analyzed"}
        }
    }
}'
Remove
-------
sudo dpkg --purge qlogtransfer
