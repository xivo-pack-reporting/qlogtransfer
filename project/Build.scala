import sbt.Keys._
import sbt._
import com.typesafe.sbt.SbtNativePackager._
import NativePackagerKeys._
import com.typesafe.sbt.packager.archetypes.ServerLoader.SystemV



object ApplicationBuild extends Build {

  val appName = "qlogtransfer"
  val appVersion = "1.5.2"
  val appOrganisation = "xivo"



  val main = Project(appName, file("."), settings = packageArchetype.java_application)
    .settings(
      name := appName,
      version := appVersion,
      scalaVersion := Version.scala,
      organization := appOrganisation,
      resolvers ++= Dependencies.resolutionRepos,
      libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep,
      publishArtifact in(Compile, packageDoc) := false,
      publishArtifact in packageDoc := false
    )
    .settings(CustomDebianSettings.debianSettings: _*)
    .settings(CustomDebianSettings.debianMappings: _*)


  object CustomDebianSettings {

    val debianSettings = Seq(
      serverLoading in Debian := SystemV,
      maintainer in Linux := "Jirka HLAVACEK <jhlavacek@avencall.com>",
      packageSummary := "Queue log transfer from Asterisk to Elasticsearch",
      packageDescription := """periodic data transfer""",
      debianChangelog := Some(file("debian/changelog"))
    )

    val debianMappings = Seq(
      linuxPackageMappings <+= (baseDirectory) map { baseDirectory =>
       (packageMapping((baseDirectory / "src/main/resources/application.conf") -> "/usr/share/qlogtransfer/conf/qlogtransfer.conf")
         withUser "qlogtransfer" withGroup "qlogtransfer" withPerms "0644")
       },

      linuxPackageMappings <+= (baseDirectory) map { baseDirectory =>
        (packageMapping((baseDirectory / "src/main/resources/qlogtransfer.cron") -> "/etc/cron.d/qlogtransfer")
          withUser "root" withGroup "root" withPerms "0644")
      },

      linuxPackageMappings <+= (baseDirectory) map { baseDirectory =>
        (packageMapping((baseDirectory / "src/main/resources/emptyLogFile") -> "/var/log/qlogtransfer.log")
          withUser "qlogtransfer" withGroup "qlogtransfer" withPerms "0644")
      }
    )
  }

}