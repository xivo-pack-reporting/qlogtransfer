import sbt._

object Version {
  val scala     = "2.10.4"
  val elastic4s = "1.0.0.0"
  val specs2    = "2.2"
  val anorm = "2.1.5"
  val jodatime = "2.0"
  val postgresql = "9.1-901.jdbc4"
  val scalike = "2.1.1"
  val dbunit = "2.4.7"
  val config = "1.2.0"
  val logback = "1.0.9"
  val jackson = "2.2.2"
}

object Library {
  val specs2            = "org.specs2"                      %% "specs2"                 % Version.specs2
  val elastic4s         = "com.sksamuel.elastic4s"          %% "elastic4s"              % Version.elastic4s
  val anorm             = "play"                            %% "anorm"                  % Version.anorm
  val scalike           = "org.scalikejdbc"                 %% "scalikejdbc"            % Version.scalike
  val postgresql        = "postgresql"                      %  "postgresql"             % Version.postgresql
  val jodatime          = "joda-time"                       %  "joda-time"              % Version.jodatime
  val dbunit            = "org.dbunit"                      %  "dbunit"                 % Version.dbunit
  val config            = "com.typesafe"                    %  "config"                 % Version.config
  val logback           = "ch.qos.logback"                  %  "logback-classic"        % Version.logback
  val jackson           = "com.fasterxml.jackson.module"    %% "jackson-module-scala"   % Version.jackson 
  val jacksondatabind   = "com.fasterxml.jackson.core"      %  "jackson-databind"       % Version.jackson 
}

object Dependencies {

  import Library._

  val runDep = List(
    elastic4s,
    anorm,
    postgresql,
    scalike,
    jodatime,
    config,
    logback,
    jackson,
    jacksondatabind
  )

  val testDep = List(
    dbunit,
    specs2
  )

  val resolutionRepos = List(
    "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
    "Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository"
  )

}
