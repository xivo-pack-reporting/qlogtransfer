package xivo

import com.typesafe.config.ConfigFactory

object Configuration {
  val conf = ConfigFactory.load
  val dbDriver = conf.getString("db.default.driver")
  val dbUrl = conf.getString("db.default.url")
  val dbUser = conf.getString("db.default.user")
  val dbPassword = conf.getString("db.default.password")
  val fetchPeriod = conf.getInt("fetch.period")
  val elasticHost = conf.getString("elastic.host")
  val elasticPort = conf.getInt("elastic.port")
  val elasticClusterName = conf.getString("elastic.cluster.name")
  val elasticDataTtl = conf.getInt("elastic.data.ttl")
}