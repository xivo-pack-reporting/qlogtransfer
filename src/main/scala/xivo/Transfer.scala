package xivo

import scalikejdbc.ConnectionPool
import xivo.data.QueueLog
import org.slf4j.LoggerFactory
import xivo.elastic.QueueLogEs

object Transfer extends App {
  
  val logger = LoggerFactory.getLogger(this.getClass)
  
  logger.info("starting queue log transfer ....")
  
  Class.forName(Configuration.dbDriver)
  ConnectionPool.singleton(Configuration.dbUrl, Configuration.dbUser, Configuration.dbPassword)
  
  
  QueueLogEs.storeQueueLogs(QueueLog.getAll)

}