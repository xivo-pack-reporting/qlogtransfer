package xivo.data

import java.util.Date
import anorm._
import anorm.SqlParser.get
import scalikejdbc.ConnectionPool
import java.sql.Connection
import org.joda.time.format.DateTimeFormat
import xivo.Configuration
import org.slf4j.LoggerFactory

case class QueueLog(queuelogid: String, queuetime: Date, callid: String,
  queuename: String, queuedisplayname: Option[String], agent: String, agentnumber: Option[String], agentname: Option[String], groupname: Option[String], event: String, data1: String)

object QueueLog {
  val logger = LoggerFactory.getLogger(this.getClass)

  val query = s"""
          select
              (ql.time ||'.'|| ql.callid) as queuelogid,
              to_char(cast(time as timestamp), 'YYYY-MM-DD HH24:MI:SS.MS') as queuetime,
              ql.callid,
              ql.queuename,
              qf.displayname as queuedisplayname,
              ql.agent,
              af.number as agentnumber,
              af.firstname || ' ' || af.lastname as agentname,
              ql.event,
              ql.data1,
              ag.name as groupname
          from queue_log as ql
          left outer join queuefeatures as qf on (ql.queuename = qf.name)
          left outer join agentfeatures as af on (ql.agent = 'Agent/' || af.number)
          left outer join agentgroup as ag on (af.numgroup = ag.id)
          where ql.time > cast((now() - interval '${Configuration.fetchPeriod} minutes') as varchar)
    """

  val simple = {
    val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss.SSS")
    get[String]("queuelogid") ~
      get[String]("queuetime") ~
      get[String]("callid") ~
      get[String]("queuename") ~
      get[Option[String]]("queuedisplayname") ~
      get[String]("agent") ~
      get[Option[String]]("agentnumber") ~
      get[Option[String]]("agentname") ~
      get[Option[String]]("groupname") ~
      get[String]("event") ~
      get[Option[String]]("queue_log.data1") map {
        case queuelogid ~ queuetime ~ callid ~ queuename ~ queuedisplayname ~ agent ~ agentnumber ~ agentname ~ groupname ~ event ~ data1 =>
          QueueLog(queuelogid, format.parseDateTime(queuetime).toDate(), callid, queuename, queuedisplayname, agent, agentnumber, agentname, groupname, event, data1.getOrElse(""))
      }
  }

  def getAll: List[QueueLog] = {
    implicit val conn: Connection = ConnectionPool.borrow()
    logger.info(s"fetching last ${Configuration.fetchPeriod} minutes of queue log")
    SQL(query).as(simple *)
  }

}
