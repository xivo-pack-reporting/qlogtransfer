package xivo.elastic

import xivo.data.QueueLog
import xivo.Configuration
import org.slf4j.LoggerFactory
import com.sksamuel.elastic4s.ElasticClient
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.mapping.FieldType._
import org.elasticsearch.search.sort.SortOrder
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.databind.JsonNode
import org.elasticsearch.common.settings.ImmutableSettings
import scala.concurrent.duration._

object QueueLogEs {
  val logger = LoggerFactory.getLogger(this.getClass)
  val DataTtl = Configuration.elasticDataTtl.days.toMillis

  def storeQueueLogs(queueLogs : List[QueueLog]) = {
    val settings = ImmutableSettings.settingsBuilder().put("cluster.name", Configuration.elasticClusterName).build()

    val client = ElasticClient.remote(settings,(Configuration.elasticHost, Configuration.elasticPort))
    logger.info(s"will send ${queueLogs.length} storing queue logs to ${Configuration.elasticHost} on cluster ${Configuration.elasticClusterName}")
    
    for(queueLog <- queueLogs) {
        client.sync.execute {
          index into "queuelogs" id queueLog.queuelogid fields (
            "queuetime" -> queueLog.queuetime,
            "callid" -> queueLog.callid,
            "queuename" -> queueLog.queuename,
            "queuedisplayname" -> queueLog.queuedisplayname.getOrElse(""),
            "agentnumber" -> queueLog.agentnumber.getOrElse(""),
            "agentname" -> queueLog.agentname.getOrElse(""),
            "groupname" -> queueLog.groupname.getOrElse(""),
            "event" -> queueLog.event,
            "data1" -> queueLog.data1
          ) ttl DataTtl
        }
    }
    logger.info(s"${queueLogs.length} imported to elastic search")
  }
  
  private def createMapping(client:ElasticClient) = {
      client.execute {
      create index "queuelogs" mappings (
        "queuelogs" as (
          "queuedisplayname" typed StringType index "not_analyzed",
          "agentname" typed StringType index "not_analyzed",
          "groupname" typed StringType index "not_analyzed")
          )
    }
  }
  
  def searchMax = {
    val client = ElasticClient.remote(Configuration.elasticHost, Configuration.elasticPort)
    val resp = client.sync.search {
        search in "queuelogs"->"queuelogs" query matchall limit 1 sort {
          by field("queuetime") order SortOrder.DESC
        }
    }
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)
    val rs = mapper.readValue(resp.toString,classOf[JsonNode])
    println(resp.toString())
    println(rs.get("hits").get("hits").get(0).get("_source"))
  }

}