drop table if exists agentfeatures;
CREATE TABLE agentfeatures
(
  id serial NOT NULL,
  numgroup integer NOT NULL,
  firstname character varying(128) NOT NULL DEFAULT ''::character varying,
  lastname character varying(128) NOT NULL DEFAULT ''::character varying,
  "number" character varying(40) NOT NULL,
  passwd character varying(128) NOT NULL,
  context character varying(39) NOT NULL,
  language character varying(20) NOT NULL,
  autologoff integer,
  "group" character varying(255) DEFAULT NULL::character varying,
  preprocess_subroutine character varying(40),
  CONSTRAINT agentfeatures_pkey PRIMARY KEY (id)
);
