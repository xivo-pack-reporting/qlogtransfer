package xivo.data

import java.sql.Connection
import org.specs2.mutable.{Specification, BeforeAfter}
import scalikejdbc._
import xivo.data.dbunit.com.sample.utils.DBUtil


class QueueLogSpec extends Specification with BeforeAfter {

  implicit var conn: Option[Connection] = None

  def before = {
    Class.forName("org.postgresql.Driver")
    ConnectionPool.singleton("jdbc:postgresql://127.0.0.1/testdata", "test", "test")
    conn = Some(ConnectionPool.borrow())
    DBUtil.setupDB("queuelog.xml")
    updateDates()
  }

  private def updateDates() = {
    DB autoCommit {implicit session =>
      sql"""update queue_log SET "time" = cast((localtimestamp -  interval '53 minutes' ) as varchar)""".execute.apply
    }
  }

  def after = conn.map(conn => conn.close())

  "queuelog" should {
    "be able to be retreived" in {
      for(queueLog <- QueueLog.getAll) println(queueLog)
      QueueLog.getAll.size shouldEqual 3
    }
  }

}