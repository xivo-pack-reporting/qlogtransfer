package xivo.data.dbunit

package com.sample.utils

import org.dbunit.dataset.{ IDataSet }
import org.dbunit.database.{ DatabaseConfig, DatabaseConnection }
import org.dbunit.ext.postgresql.PostgresqlDataTypeFactory
import org.dbunit.operation.{ DatabaseOperation }
import java.sql.Connection
import scalikejdbc.ConnectionPool
import org.dbunit.dataset.xml.FlatXmlDataSet
import anorm._

object DBUtil {
  private var dataset: IDataSet = null
  private var dbunitConnection: DatabaseConnection = null

  def setupDB(filename: String) = {
    try {
        implicit val conn: Connection = ConnectionPool.borrow()
        this.dataset = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream(filename))
        for (table <- this.dataset.getTableNames()) {
          val createTable =scala.io.Source.fromInputStream(this.getClass().getClassLoader().getResourceAsStream(s"${table}.sql")).mkString
          SQL(createTable).execute
        }
    
        this.dbunitConnection = new DatabaseConnection(conn)
        val dbConfig = this.dbunitConnection.getConfig()
        dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new PostgresqlDataTypeFactory)
        DatabaseOperation.CLEAN_INSERT.execute(this.dbunitConnection, this.dataset)
    }
    catch {
      case e: Exception => println("-------------------"+e.getMessage())
    }
  }

  def shutdownDB = {
    implicit val conn: Connection = ConnectionPool.borrow()
    DatabaseOperation.DELETE.execute(this.dbunitConnection, this.dataset)

  }
}